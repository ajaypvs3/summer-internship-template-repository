## Team 7 ## AI enabled cart conversion 

The participants are required to clone this repository and create a private Gitlab repository under their own username (Single repository per team). The following created sections in this README.md need to be duly filled, highlighting the denoted points for the solution/implementation. Please feel free to create further sub-sections in this markdown, the idea is to understand the gist of the components in a singular document.

### business model for ongoing e-commerce sites
----------------------------------

A brief description of 
* What problem did the team try to solve
* What is the proposed solution

### customer based analysis of cart conversion.
### friendly checkout system----------------------------------

#### Architecture Diagram

Affix an image of the flow diagram/architecture diagram of the solution

#### 

An overview of 
* What technologies/versions were used
* Setup/Installations required to run the solution
* Instructions to run the submitted code

### bharath reddy, manogna, ajay, bhavitha
----------------------------------

List of team member names and email IDs with their contributions
